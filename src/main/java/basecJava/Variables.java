package basecJava;

public class Variables {

    int size; // этот вариант более "говорящий", понятно из названия, что переменная отвечает за размер чего-то
    int numberOfApples = 5; // объявили переменную количество яблок типа int присваиваем значение 5

    float money = 10.5f; // объявили переменную money типа float присваиваем значение 10.5
    double salary = 100.45d; // объявили переменную salary типа double присваиваем значение 100.45d

    char letter = 'a'; // объявили переменную letter типа char присваиваем значение 'a'

    boolean man = true; // объявили переменную man типа boolean присваиваем значение true
    boolean woman = false; // объявили переменную woman типа boolean присваиваем значение false

    String title = "Обожаю Java"; // объявили переменную title типа String присваиваем значение "Обожаю Java"

}
