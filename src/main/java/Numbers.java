public class Numbers {


    public int getSum(int N) {
        int result = 0;
        for (int i = 1; i <= N; i++)
            result = result + i;
        return result;
    }

    //Площадь прямоугольника
    public int calcRectangleSquare(int x, int y) {
        return x * y;
    }

    //Площадь квадрата
    public int calcSquare(int x) {
        return x * x;
    }

}
