public class Calculator {

    //Сложение чисел
    public int additionOfNumbers(int firstNumber, int secondNumber) {
        return firstNumber + secondNumber;
    }

    //Вычитание чисел
    public int numberSubtraction(int firstNumber, int secondNumber) {
        return firstNumber - secondNumber;
    }

    //Умножение чисел
    public int multiplicationOfNumbers(int firstNumber, int secondNumber) {
        return firstNumber * secondNumber;
    }

    //Деление чисел
    public int divisionOfNumbers(int firstNumber, int secondNumber) {
        return firstNumber / secondNumber;
    }

}
