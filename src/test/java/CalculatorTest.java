import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CalculatorTest {

    @Test
    void additionOfNumbers() {
        Calculator calculator = new Calculator();
        int actualValue = calculator.additionOfNumbers(5, 4);
        int expectValue = 9;
        Assertions.assertEquals(expectValue, actualValue, "Некорректное значение - " + actualValue);
    }

    @Test
    void numberSubtraction() {
        Calculator calculator = new Calculator();
        int actualValue = calculator.numberSubtraction(5, 4);
        int expectValue = 1;
        Assertions.assertEquals(expectValue, actualValue, "Некорректное значение - " + actualValue);
    }

    @Test
    void multiplicationOfNumbers() {
        Calculator calculator = new Calculator();
        int actualValue = calculator.multiplicationOfNumbers(5, 4);
        int expectValue = 20;
        Assertions.assertEquals(expectValue, actualValue, "Некорректное значение - " + actualValue);
    }

    @Test
    void divisionOfNumbers() {
        Calculator calculator = new Calculator();
        int actualValue = calculator.divisionOfNumbers(20, 4);
        int expectValue = 5;
        Assertions.assertEquals(expectValue, actualValue, "Некорректное значение - " + actualValue);
    }

}