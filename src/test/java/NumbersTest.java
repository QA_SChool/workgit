import org.junit.jupiter.api.Assertions;

class NumbersTest {

    @org.junit.jupiter.api.Test
    void getSum() {
        Numbers numbers = new Numbers();
        int actual = numbers.getSum(5);
        int expect = 15;
        Assertions.assertEquals(actual, expect, "Некорректное значение - " + actual);
    }

    @org.junit.jupiter.api.Test
    void calcRectangleSquare() {
        Numbers numbers = new Numbers();
        int actual = numbers.calcRectangleSquare(5, 3);
        int expect = 15;
        Assertions.assertEquals(actual, expect, "Некорректное значение - " + actual);
    }

    @org.junit.jupiter.api.Test
    void calcSquare() {
        Numbers numbers = new Numbers();
        int actual = numbers.calcSquare(5);
        int expect = 25;
        Assertions.assertEquals(actual, expect, "Некорректное значение - " + actual);
    }

}